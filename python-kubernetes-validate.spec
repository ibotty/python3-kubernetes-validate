Name:           python-kubernetes-validate
Version:        1.23.1
Release:        0%{?dist}
Summary:        kubernetes-validate is a library that validates Kubernetes resource definitions.

License:        ASL 2.0
URL:            https://github.com/willthames/kubernetes-validate
Source0:        %{url}/archive/v%{version}/kubernetes-validate-%{version}.tar.gz

BuildArch:      noarch
BuildRequires:  python3-devel

%global _description %{expand:
kubernetes-validate validates Kubernetes resource definitions against the declared Kubernetes schemas.}

%description %_description

%package -n python3-kubernetes-validate
Summary:        %{summary}

%description -n python3-kubernetes-validate %_description


%prep
%autosetup -p1 -n kubernetes-validate-%{version}

%generate_buildrequires
%pyproject_buildrequires -t


%build
%pyproject_wheel


%install
%pyproject_install

# Here, "pello" is the name of the importable module.
%pyproject_save_files kubernetes_validate


%check
%tox


%files -n python3-kubernetes-validate -f %{pyproject_files}
%doc README.md
%{_bindir}/kubernetes-validate


%changelog
* Fri Mar 11 2022 Tobias Florek <me@ibotty.net> - 1.23.1
- initial packaging
